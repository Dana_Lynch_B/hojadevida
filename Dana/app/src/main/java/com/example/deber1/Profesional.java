package com.example.deber1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Profesional extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profesional);
    }
    public void Regresar(View view) {
        Intent intent = new Intent(this, ReferenciasActivity2.class);
        startActivity(intent);
    }
}